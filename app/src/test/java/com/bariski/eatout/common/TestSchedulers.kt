package com.bariski.eatout.common

import com.bariski.eatout.domain.repository.Schedulers
import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler

class TestSchedulers(private val testScheduler: TestScheduler) : Schedulers {
    override fun io(): Scheduler {
        return testScheduler
    }

    override fun ui(): Scheduler {
        return testScheduler
    }

    override fun compute(): Scheduler {
        return testScheduler
    }
}