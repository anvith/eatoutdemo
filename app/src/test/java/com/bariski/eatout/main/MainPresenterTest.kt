package com.bariski.eatout.main

import android.content.Context
import com.bariski.eatout.R
import com.bariski.eatout.common.TestSchedulers
import com.bariski.eatout.domain.repository.*
import com.bariski.eatout.presentation.di.GoogleApiServices
import com.bariski.eatout.presentation.main.MainContract
import com.bariski.eatout.presentation.main.MainPresenter
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.`when`

class MainPresenterTest {

    private val view: MainContract.View = mock()
    private val repository: RestaurantRepository = mock()
    private val resourceProvider: ResourceProviderRepository = mock()
    private val deviceStoreRepository: DeviceStoreRepository = mock()
    private val userRepository: UserRepository = mock()
    private val locationRepository: LocationRepository = mock()
    private val clientId = "clientId"
    private val clientSecret = "clientSecret"
    private val context: Context = mock()
    private val googleApiClient: GoogleApiClient = mock()
    private val googleApiServices: GoogleApiServices = mock()
    private lateinit var presenter: MainContract.Presenter
    private lateinit var testScheduler: TestScheduler
    private val authRequest = Observable.empty<Any>()

    private val AWAIT = "await"
    private val GET_ADDRESS = "get"
    private val ERROR = "error"


    @Before
    fun setup() {
        testScheduler = TestScheduler()
        val testSchedulerProvider = TestSchedulers(testScheduler)
        presenter = MainPresenter(repository, testSchedulerProvider, resourceProvider, locationRepository, deviceStoreRepository, userRepository, clientId, clientSecret, context, googleApiServices)
        `when`(view.getUserAuthRequest()).thenReturn(authRequest)
        `when`(view.isLocationSettingsEnabled()).thenReturn(false)
        `when`(resourceProvider.getString(R.string.location_status_awaiting)).thenReturn(AWAIT)
        `when`(googleApiServices.getGoogleApiClient(context, presenter, presenter, LocationServices.API)).thenReturn(googleApiClient)
    }

    @Test
    fun `initializing presenter sets initial ui`() {
        `when`(userRepository.isLoggedIn()).thenReturn(false)
        presenter.initView(view, null)
        verify(view).updateLocationStatus(AWAIT, true)
        verify(view).toggleAuthUi(false)
    }

    @Test
    fun `auth initiation for nonauth user initiates login`() {
        `when`(view.getUserAuthRequest()).thenReturn(Observable.just(1))
        `when`(userRepository.isLoggedIn()).thenReturn(false)
        presenter.initView(view, null)
        testScheduler.triggerActions()
        verify(view).makeLoginRequest(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())
    }

    @Test
    fun `auth initiation for auth user initiates logsout`() {
        `when`(view.getUserAuthRequest()).thenReturn(Observable.just(1))
        `when`(userRepository.isLoggedIn()).thenReturn(true)
        presenter.initView(view, null)
        testScheduler.triggerActions()
        verify(userRepository).setOAuthToken(ArgumentMatchers.matches(""))
        verify(view).toggleAuthUi(false)
    }

}