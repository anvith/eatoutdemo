package com.bariski.eatout.data.repository

import android.content.Context
import com.bariski.eatout.domain.repository.ResourceProviderRepository

class ResourceProviderRepositoryImpl(val context: Context) : ResourceProviderRepository {
    override fun getString(id: Int): String {
        return context.getString(id)
    }
}