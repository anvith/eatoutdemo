package com.bariski.eatout.data.di

import com.bariski.eatout.data.api.RestaurantApi
import com.bariski.eatout.data.repository.RestaurantRepositoryImpl
import com.bariski.eatout.domain.repository.RestaurantRepository
import com.bariski.eatout.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class RestaurantsModule {

    @Provides
    fun provideRestaurantRepository(api: RestaurantApi, userRepository: UserRepository): RestaurantRepository {
        return RestaurantRepositoryImpl(api, userRepository)
    }

    @Provides
    fun provideRestaurantApi(retrofit: Retrofit): RestaurantApi {
        return retrofit.create(RestaurantApi::class.java)
    }


}