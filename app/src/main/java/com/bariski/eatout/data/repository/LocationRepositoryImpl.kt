package com.bariski.eatout.data.repository

import android.location.Address
import android.location.Geocoder
import android.location.Location
import com.bariski.eatout.domain.repository.LocationRepository
import io.reactivex.Single

class LocationRepositoryImpl(val geocoder: Geocoder) : LocationRepository {
    override fun getAddressFromLocation(location: Location): Single<Address?> {
        return Single.just(location)
                .map { geocoder.getFromLocation(it.latitude, it.longitude, 1) }
                .map {
                    if (it.size > 0) {
                        it[0]
                    } else {
                        null
                    }
                }
    }
}