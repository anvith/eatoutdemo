package com.bariski.eatout.data.di

import com.bariski.eatout.di.ApplicationScope
import com.bariski.eatout.domain.repository.Schedulers
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

@Module
class SchedulerModule {

    @Provides
    @ApplicationScope
    fun provideScheduler(): Schedulers {
        return object : Schedulers {
            override fun io(): Scheduler {
                return io.reactivex.schedulers.Schedulers.io()
            }

            override fun ui(): Scheduler {
                return AndroidSchedulers.mainThread()
            }

            override fun compute(): Scheduler {
                return io.reactivex.schedulers.Schedulers.computation()
            }

        }
    }
}