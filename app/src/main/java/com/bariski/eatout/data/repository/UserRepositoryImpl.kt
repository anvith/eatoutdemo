package com.bariski.eatout.data.repository

import com.bariski.eatout.domain.repository.KeyValueStore
import com.bariski.eatout.domain.repository.UserRepository
import com.bariski.eatout.presentation.common.EMPTY_STRING

class UserRepositoryImpl(val keyValueStore: KeyValueStore) : UserRepository {

    private val AUTH_TOKEN = "AUTH_TOKEN"

    override fun isLoggedIn() = getOAuthToken().isNotEmpty()

    override fun setOAuthToken(value: String) {
        keyValueStore.storeString(AUTH_TOKEN, value)
    }

    override fun getOAuthToken(): String {
        return keyValueStore.getString(AUTH_TOKEN) ?: EMPTY_STRING
    }
}