package com.bariski.eatout.data.di

import com.bariski.eatout.data.repository.UserRepositoryImpl
import com.bariski.eatout.di.ApplicationScope
import com.bariski.eatout.domain.repository.KeyValueStore
import com.bariski.eatout.domain.repository.UserRepository
import dagger.Module
import dagger.Provides

@Module
class UserModule {

    @Provides
    @ApplicationScope
    fun provideUserRepository(keyValueStore: KeyValueStore): UserRepository {
        return UserRepositoryImpl(keyValueStore)
    }

}