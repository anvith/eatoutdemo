package com.bariski.eatout.data.repository

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import com.bariski.eatout.BuildConfig
import com.bariski.eatout.R
import com.bariski.eatout.domain.models.ImageRequest
import com.bariski.eatout.domain.repository.ImageLoaderRepository
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.*


class ImageLoaderRepositoryImpl(val context: Context) : ImageLoaderRepository {

    val TAG = "ImageRepository"
    val cache: Cache


    init {
        val builder = Picasso.Builder(context)
        cache = LruCache(context)
        builder.memoryCache(LruCache(context))
        builder.downloader(OkHttp3Downloader(context, Integer.MAX_VALUE.toLong()))
        val built = builder.build()
        built.isLoggingEnabled = BuildConfig.DEBUG
        Picasso.setSingletonInstance(built)
    }

    override fun loadImage(request: ImageRequest) {

        var context: Context? = null
        if (request.activity != null && !request.activity.isDestroyed) {
            context = request.activity
        } else if (request.fragment != null && request.fragment.activity != null && !request.fragment.activity.isDestroyed) {
            context = request.fragment.activity
        }

        if (context != null) {
            var creator = getCreator(request, context)
            if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                creator = creator.networkPolicy(NetworkPolicy.OFFLINE)
            }
            creator.into(request.target, object : Callback {
                override fun onSuccess() {

                }

                override fun onError() {
                    var creator = getCreator(request, context)
                    creator.into(request.target)
                }
            })
        }
    }

    private fun getCreator(request: ImageRequest, context: Context): RequestCreator {
        var creator = if (request.url.trim().isNotEmpty()) {
            Picasso.with(context).load(request.url)
        } else {
            Picasso.with(context).load(R.drawable.placeholder)
        }
        request.placeholder?.let {
            creator = creator.placeholder(it)
        }
        request.errorImage?.let {
            creator.error(it)
        }
        return creator
    }

    override fun clearCache() {
        cache.clear()
    }
}
