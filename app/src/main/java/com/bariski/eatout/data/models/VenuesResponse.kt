package com.bariski.eatout.data.models

import com.bariski.eatout.presentation.models.Restaurant


data class VenuesResponse(val response: Response?)

data class Response(val groups: List<Group>?)

data class Group(val name: String?, val type: String?, val items: List<Item>?)

data class Item(val venue: Restaurant)
