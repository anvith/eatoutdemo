package com.bariski.eatout.data.repository

import com.bariski.eatout.domain.repository.DeviceStoreRepository
import com.bariski.eatout.domain.repository.KeyValueStore

class DeviceStoreRepositoryImpl(val keyValueStore: KeyValueStore) : DeviceStoreRepository {

    private val LOCATION_RATIONALE_SHOWN = "LOCATION_RATIONALE_SHOWN"

    override fun hasLocationRationaleBeenShown() = keyValueStore.getBoolean(LOCATION_RATIONALE_SHOWN)

    override fun setLocationRationaleShown(value: Boolean) {
        keyValueStore.storeBoolean(LOCATION_RATIONALE_SHOWN, value)
    }
}