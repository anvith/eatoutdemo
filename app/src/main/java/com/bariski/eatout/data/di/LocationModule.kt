package com.bariski.eatout.data.di

import android.content.Context
import android.location.Geocoder
import com.bariski.eatout.data.repository.LocationRepositoryImpl
import com.bariski.eatout.di.ApplicationScope
import com.bariski.eatout.domain.repository.LocationRepository
import dagger.Module
import dagger.Provides

@Module
class LocationModule {

    @Provides
    @ApplicationScope
    fun provideLocationRepository(geocoder: Geocoder): LocationRepository {
        return LocationRepositoryImpl(geocoder)
    }

    @Provides
    @ApplicationScope
    fun provideGeocoder(context: Context): Geocoder {
        return Geocoder(context)
    }
}