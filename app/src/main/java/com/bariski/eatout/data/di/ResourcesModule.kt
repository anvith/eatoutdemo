package com.bariski.eatout.data.di

import android.content.Context
import com.bariski.eatout.data.repository.ResourceProviderRepositoryImpl
import com.bariski.eatout.di.ApplicationScope
import com.bariski.eatout.domain.repository.ResourceProviderRepository
import dagger.Module
import dagger.Provides


@Module
class ResourcesModule {

    @ApplicationScope
    @Provides
    fun provideResourceProvider(context: Context): ResourceProviderRepository {
        return ResourceProviderRepositoryImpl(context)
    }
}