package com.bariski.eatout.data.di

import android.content.Context
import com.bariski.eatout.BuildConfig
import com.bariski.eatout.FoodOrderApplication
import com.bariski.eatout.data.logging.HttpLoggingInterceptor
import com.bariski.eatout.data.repository.ImageLoaderRepositoryImpl
import com.bariski.eatout.di.AppModule
import com.bariski.eatout.di.ApplicationScope
import com.bariski.eatout.domain.repository.ImageLoaderRepository
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.*
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Named


@Module(includes = [AppModule::class])
class NetworkModule {

    @Named(BASE_URL_KEY)
    @Provides
    fun provideBaseUrl(): String {
        return BuildConfig.BASE_URL
    }

    @Named(CLIENT_ID)
    @Provides
    fun provideClientId(): String {
        return BuildConfig.CLIENT_ID
    }

    @Named(CLIENT_SECRET)
    @Provides
    fun provideClientSecret(): String {
        return BuildConfig.CLIENT_SECRET
    }

    @Provides
    @ApplicationScope
    fun provideRetrofit(@Named(BASE_URL_KEY) baseUrl: String, okHttpClient: OkHttpClient, converterFactory: MoshiConverterFactory, callAdapterFactory: CallAdapter.Factory): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .build()

    }

    @Provides
    @ApplicationScope
    fun provideOkHttpClient(requestInterceptor: Interceptor): OkHttpClient {
        val spec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_RC4_128_SHA,
                        CipherSuite.TLS_ECDHE_RSA_WITH_RC4_128_SHA,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_DHE_DSS_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA)
                .build()
        return OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addNetworkInterceptor(requestInterceptor)
                .connectionSpecs(Collections.singletonList(spec))
                .build()

    }

    @Provides
    @ApplicationScope
    fun provideNetworkInterceptor(): Interceptor {
        return HttpLoggingInterceptor().setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
    }

    @ApplicationScope
    @Provides
    fun provideHttpCache(application: FoodOrderApplication): Cache {
        val cacheSize = 10 * 1024 * 1024L
        return Cache(application.cacheDir, cacheSize)
    }

    @Provides
    @ApplicationScope
    fun provideConverter(): MoshiConverterFactory {
        return MoshiConverterFactory.create(Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build())
    }

    @Provides
    @ApplicationScope
    fun provideCallAdapter(): CallAdapter.Factory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    @ApplicationScope
    fun provideImageLoader(context: Context): ImageLoaderRepository {
        return ImageLoaderRepositoryImpl(context)
    }

    @Provides
    @ApplicationScope
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
    }

}