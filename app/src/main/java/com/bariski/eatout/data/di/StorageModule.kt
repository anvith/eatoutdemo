package com.bariski.eatout.data.di

import android.content.Context
import com.bariski.eatout.data.repository.DeviceStoreRepositoryImpl
import com.bariski.eatout.data.repository.KeyValueStoreImpl
import com.bariski.eatout.di.ApplicationScope
import com.bariski.eatout.domain.repository.DeviceStoreRepository
import com.bariski.eatout.domain.repository.KeyValueStore
import dagger.Module
import dagger.Provides

@Module
class StorageModule {

    @Provides
    @ApplicationScope
    fun provideKeyValueStore(context: Context): KeyValueStore {
        return KeyValueStoreImpl(context)
    }

    @Provides
    @ApplicationScope
    fun provideDeviceStore(keyValueStore: KeyValueStore): DeviceStoreRepository {
        return DeviceStoreRepositoryImpl(keyValueStore)
    }

}