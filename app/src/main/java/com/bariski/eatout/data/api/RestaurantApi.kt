package com.bariski.eatout.data.api

import com.bariski.eatout.BuildConfig
import com.bariski.eatout.data.models.VenuesResponse
import com.bariski.eatout.presentation.common.DATE_FOURSQUARE_API
import com.bariski.eatout.presentation.common.SEARCH_RADIUS
import com.bariski.eatout.presentation.common.SECTION_FOOD
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface RestaurantApi {

    @GET("venues/explore?section=$SECTION_FOOD&client_id=${BuildConfig.CLIENT_ID}&client_secret=${BuildConfig.CLIENT_SECRET}&v=$DATE_FOURSQUARE_API&radius=$SEARCH_RADIUS&sortByDistance=1&limit=50")
    fun getNearbyRestaurants(@Query("ll") latLong: String): Single<VenuesResponse>

    @GET("venues/explore?section=$SECTION_FOOD&v=$DATE_FOURSQUARE_API&radius=$SEARCH_RADIUS&sortByDistance=1&limit=50")
    fun getNearbyRestaurantsForUser(@Query("ll") latLong: String, @Query("oauth_token") token: String): Single<VenuesResponse>

    @POST("venues/{venueId}/dislike?v=$DATE_FOURSQUARE_API")
    fun performDislike(@Path("venueId") venueId: String, @Query("oauth_token") token: String): Completable

}