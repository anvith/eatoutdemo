package com.bariski.eatout.data.di

const val BASE_URL_KEY = "BASE_URL_KEY"

const val CLIENT_ID = "CLIENT_ID"
const val CLIENT_SECRET = "CLIENT_SECRET"