package com.bariski.eatout.data.repository

import com.bariski.eatout.data.api.RestaurantApi
import com.bariski.eatout.domain.repository.RestaurantRepository
import com.bariski.eatout.domain.repository.UserRepository
import com.bariski.eatout.presentation.models.Restaurant
import io.reactivex.Completable
import io.reactivex.Single

class RestaurantRepositoryImpl(val restaurantApi: RestaurantApi, val userRepository: UserRepository) : RestaurantRepository {

    override fun dislikeVenue(venueId: String): Completable {
        return restaurantApi.performDislike(venueId, userRepository.getOAuthToken())
    }

    override fun getRestaurants(latLong: String): Single<List<Restaurant>> {

        val restaurantsSingle = if (userRepository.isLoggedIn()) {
            restaurantApi.getNearbyRestaurantsForUser(latLong, userRepository.getOAuthToken())
        } else {
            restaurantApi.getNearbyRestaurants(latLong)
        }

        return restaurantsSingle.map {
            val list = ArrayList<Restaurant>()
            it.response?.groups?.forEach {
                it.items?.forEach {
                    list.add(it.venue)
                }
            }
            list
        }
    }

}