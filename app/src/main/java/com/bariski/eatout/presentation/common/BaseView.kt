package com.bariski.eatout.presentation.common

interface BaseView<T> {
    fun getPresenter(): T
}