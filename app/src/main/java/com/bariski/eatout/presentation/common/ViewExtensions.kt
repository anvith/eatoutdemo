package com.bariski.eatout.presentation.common

import android.view.View

fun View.toggleVisiblilty(makeVisible: Boolean) {
    this.visibility = if (makeVisible) View.VISIBLE else View.GONE
}