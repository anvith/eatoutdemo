package com.bariski.eatout.presentation.models

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(
        val lat: Double,
        val lng: Double,
        val distance: Int,
        val city: String?,
        val state: String?,
        val country: String?,
        val formattedAddress: List<String>?
) : Parcelable {

    //Memoized property
    @IgnoredOnParcel
    val address: String by lazy {
        var add = ""
        if (formattedAddress != null && formattedAddress.isNotEmpty()) {
            add = formattedAddress[0]
        } else if (city != null) {
            add = city
        }
        add
    }

    @IgnoredOnParcel
    val distanceString: String by lazy {
        if (distance > 1000) {
            "%.2f".format(distance / 1000f) + " km"
        } else {
            "$distance m"
        }
    }


}