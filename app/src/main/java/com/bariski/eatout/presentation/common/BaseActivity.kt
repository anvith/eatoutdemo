package com.bariski.eatout.presentation.common

import android.os.Bundle
import android.support.v7.app.AppCompatDelegate
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

open class BaseActivity<T : Any> : DaggerAppCompatActivity(), BaseView<T> {
    @Inject
    lateinit var viewPresenter: T

    override fun getPresenter(): T = viewPresenter

    var wasDestroyed =false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    fun isAlive(): Boolean {
        return !isFinishing && !wasDestroyed
    }

    override fun onDestroy() {
        super.onDestroy()
        wasDestroyed=true
    }
}