package com.bariski.eatout.presentation.common

import com.bariski.eatout.R
import com.bariski.eatout.domain.repository.ResourceProviderRepository
import com.bariski.eatout.presentation.models.AddressResponse
import com.bariski.eatout.presentation.models.RestaurantResponse
import io.reactivex.SingleTransformer
import retrofit2.HttpException
import java.io.IOException

/**
 * Error wrapper that gracefully handles non breaking  stream exceptions
 ***/

fun applyTransformerForWrappingExpectedErrorsinVenues(resourceProvider: ResourceProviderRepository): SingleTransformer<RestaurantResponse, RestaurantResponse> {
    return SingleTransformer { upstream ->
        upstream.onErrorReturn {
            when (it) {
                is IOException -> RestaurantResponse(false, null, resourceProvider.getString(R.string.error_network_title), resourceProvider.getString(R.string.error_network_message))
                is HttpException -> RestaurantResponse(false, null, resourceProvider.getString(R.string.error_network_title), resourceProvider.getString(R.string.error_server_message))
                else -> throw  it
            }

        }
    }
}


fun applyTransformerForWrappingExpectedErrorsinAddress(): SingleTransformer<AddressResponse, AddressResponse> {
    return SingleTransformer { upstream ->
        upstream.onErrorReturn {
            when (it) {
                is IOException -> AddressResponse(false, null)
                is HttpException -> AddressResponse(false, null)
                else -> throw  it
            }

        }
    }
}


