package com.bariski.eatout.presentation.di

import com.bariski.eatout.di.ActivityScope
import com.bariski.eatout.presentation.main.MainContract
import com.bariski.eatout.presentation.main.MainPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class MainModule {

    @ActivityScope
    @Binds
    abstract fun mainPresenter(presenter: MainPresenter): MainContract.Presenter

    @ActivityScope
    @Binds
    abstract fun googleApiProvider(googleApiServicesImpl: GoogleApiServicesImpl): GoogleApiServices

}