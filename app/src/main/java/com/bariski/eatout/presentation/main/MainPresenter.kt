package com.bariski.eatout.presentation.main

import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.util.Log
import com.bariski.eatout.R
import com.bariski.eatout.data.di.CLIENT_ID
import com.bariski.eatout.data.di.CLIENT_SECRET
import com.bariski.eatout.domain.repository.*
import com.bariski.eatout.presentation.common.EMPTY_STRING
import com.bariski.eatout.presentation.common.applyTransformerForWrappingExpectedErrorsinAddress
import com.bariski.eatout.presentation.common.applyTransformerForWrappingExpectedErrorsinVenues
import com.bariski.eatout.presentation.di.GoogleApiServices
import com.bariski.eatout.presentation.models.AddressResponse
import com.bariski.eatout.presentation.models.Restaurant
import com.bariski.eatout.presentation.models.RestaurantResponse
import com.foursquare.android.nativeoauth.FoursquareOAuth
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject
import javax.inject.Named


class MainPresenter @Inject constructor(val repository: RestaurantRepository,
                                        val schedulers: Schedulers,
                                        val resourceProvider: ResourceProviderRepository,
                                        val locationRepository: LocationRepository,
                                        deviceStoreRepository: DeviceStoreRepository,
                                        val userRepository: UserRepository,
                                        @Named(CLIENT_ID) val clientId: String,
                                        @Named(CLIENT_SECRET) val clientSecret: String,
                                        context: Context,
                                        googleApiServices: GoogleApiServices
) : LocationPresenter(context, deviceStoreRepository, googleApiServices) {

    private lateinit var location: Location

    private val REQUEST_CODE_FSQ_CONNECT = 112
    private val REQUEST_CODE_FSQ_TOKEN_EXCHANGE = 113
    //Stores a list of elements pending for auth in order to execute downvote
    private var awaitingDownvoteList = ArrayList<Restaurant>()


    override fun initView(view: MainContract.View, savedState: Bundle?) {
        super.initView(view, savedState)
        view?.let {
            it.updateLocationStatus(resourceProvider.getString(R.string.location_status_awaiting), true)
            it.toggleAuthUi(userRepository.isLoggedIn())
        }
        bindAuthEvent()
        restoreStateElseRequestLocation(savedState)
    }

    private fun bindAuthEvent() {
        view?.let {
            disposable.add(it.getUserAuthRequest()
                    .subscribeOn(schedulers.ui())
                    .observeOn(schedulers.ui())
                    .subscribeBy(onNext = {
                        if (!userRepository.isLoggedIn()) {
                            view?.apply {
                                makeLoginRequest(clientId, REQUEST_CODE_FSQ_CONNECT)
                            }
                        } else {
                            userRepository.setOAuthToken(EMPTY_STRING)
                            view?.toggleAuthUi(false)
                        }

                    }, onComplete = {}, onError = {
                        Log.e(TAG, it.toString())
                    }))
        }

    }

    private fun restoreStateElseRequestLocation(savedState: Bundle?) {
        var locationStatus = resourceProvider.getString(R.string.location_status_awaiting)
        var restaurants = ArrayList<Restaurant>()
        savedState?.apply {
            restaurants = getParcelableArrayList("restaurants") ?: ArrayList()
            if (containsKey("locationStatus")) {
                locationStatus = getString("locationStatus")
            }
        }
        if (restaurants.size > 0) {
            view?.apply {
                toggleMainProgress(false)
                showRestaurants(restaurants, false)
                updateLocationStatus(locationStatus, false)
            }

        } else {
            initLocationRequestFlow()
        }
    }

    private fun loadRestaurants() {
        disposable.add(Single.just(location)
                .map { "${it.latitude},${it.longitude}" }
                .flatMap { repository.getRestaurants(it) }
                .subscribeOn(schedulers.io())
                .map { RestaurantResponse(true, it) }
                .observeOn(schedulers.ui())
                .doOnSubscribe {
                    view?.apply {
                        toggleMainProgress(true)
                        toggleError(false)
                    }
                }
                .compose(applyTransformerForWrappingExpectedErrorsinVenues(resourceProvider))
                .subscribeBy(onSuccess = {
                    view?.apply {
                        toggleMainProgress(false)
                        if (it.isSuccess) {
                            if (it.data != null && it.data.isNotEmpty()) {
                                showRestaurants(it.data, false)
                            } else {
                                toggleError(true, resourceProvider.getString(R.string.error_network_title), resourceProvider.getString(R.string.error_recommendations_empty))
                            }
                        } else {
                            toggleError(true, it.errorTitle, it.errorMessage)
                        }
                    }
                }, onError = {
                    Log.e(TAG, it.toString())
                })
        )
    }

    private fun loadAddressInfo() {
        disposable.add(locationRepository.getAddressFromLocation(location)
                .subscribeOn(schedulers.io())
                .map { AddressResponse(true, it) }
                .observeOn(schedulers.ui())
                .doOnSubscribe {
                    view?.apply {
                        updateLocationStatus(resourceProvider.getString(R.string.location_status_retrieving), true)
                    }
                }
                .compose(applyTransformerForWrappingExpectedErrorsinAddress())
                .subscribeBy(onSuccess = {
                    view?.apply {
                        if (it.isSuccess) {
                            it.data?.let {
                                updateLocationStatus(it.getAddressLine(0), false)
                            }
                        } else {
                            updateLocationStatus(resourceProvider.getString(R.string.error_location_status), false)
                        }
                    }
                }, onError = {
                    Log.e(TAG, it.toString())
                })
        )
    }

    override fun onRestaurantThumbsDowned(restaurant: Restaurant) {
        if (userRepository.isLoggedIn()) {
            disposable.add(repository.dislikeVenue(restaurant.id)
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribeBy(onError = {
                        view?.removeRestaurantFromViewFailed(restaurant)
                    }, onComplete = {
                        view?.removeRestaurantFromView(restaurant)
                    }))
        } else {
            //Add downvote to the pending queue that would be initiated once we log the user in
            awaitingDownvoteList.add(restaurant)
            view?.makeLoginRequest(clientId, REQUEST_CODE_FSQ_CONNECT)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_FSQ_CONNECT -> {
                val codeResponse = FoursquareOAuth.getAuthCodeFromResult(resultCode, data)
                val exception = codeResponse.exception
                if (exception != null) {
                    view?.showMessage(resourceProvider.getString(R.string.error_login_failure))
                    if (awaitingDownvoteList.size > 0) {
                        notifyAuthAwaitFailure()
                    }
                } else {
                    view?.performTokenExchange(codeResponse.code, REQUEST_CODE_FSQ_TOKEN_EXCHANGE, clientId, clientSecret)
                }

            }
            REQUEST_CODE_FSQ_TOKEN_EXCHANGE -> {
                val tokenResponse = FoursquareOAuth.getTokenFromResult(resultCode, data)
                val exception = tokenResponse.exception
                if (exception != null) {
                    view?.showMessage(resourceProvider.getString(R.string.error_login_failure))
                    if (awaitingDownvoteList.size > 0) {
                        notifyAuthAwaitFailure()
                    }
                } else {
                    userRepository.setOAuthToken(tokenResponse.accessToken)
                    view?.apply {
                        showMessage(resourceProvider.getString(R.string.login_success))
                        toggleAuthUi(true)
                        initLocationRequestFlow()
                    }
                    if (awaitingDownvoteList.size > 0) {
                        awaitingDownvoteList.forEach {
                            onRestaurantThumbsDowned(it)
                        }
                        awaitingDownvoteList.clear()
                    }
                }
            }
        }
    }


    override fun onLocationReceived(location: Location) {
        this.location = location
        loadRestaurants()
        loadAddressInfo()
    }

    //Function that informs UI about auth failure that resulted in failure to downvote
    private fun notifyAuthAwaitFailure() {
        awaitingDownvoteList.forEach {
            view?.removeRestaurantFromViewFailed(it)
        }
        awaitingDownvoteList.clear()
    }


    override fun onRefresh() {
        if (location != null) {
            loadRestaurants()
        } else {
            initLocationRequestFlow()
        }
    }

    override fun setLocationRationaleShown(value: Boolean) = deviceStoreRepository.setLocationRationaleShown(value)

    override fun hasLocationRationaleBeenShown() = deviceStoreRepository.hasLocationRationaleBeenShown()
}