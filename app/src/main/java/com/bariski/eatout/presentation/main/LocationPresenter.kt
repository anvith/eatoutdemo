package com.bariski.eatout.presentation.main

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import com.bariski.eatout.domain.repository.DeviceStoreRepository
import com.bariski.eatout.presentation.di.GoogleApiServices
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

abstract class LocationPresenter(val context: Context,
                                 val deviceStoreRepository: DeviceStoreRepository,
                                 val googleApiServices: GoogleApiServices) : MainContract.Presenter(),
        MainContract.LocationPresenter {

    var locationView: MainContract.LocationView? = null
    protected val TAG = "LocationActivity"
    protected val REQUEST_CHECK_SETTINGS = 0x1

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private val PERMISSIONS_REQUEST_LOCATION = 111

    val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2

    protected lateinit var googleApiClient: GoogleApiClient

    protected var locationRequest: LocationRequest? = null

    private var hasRequestedLocation: Boolean = false

    private var isLocationRequestInProgress: Boolean = false

    private var isLocationRequestAwaitingConnection: Boolean = false

    private var lastLocation: Location? = null

    private var isPermissionRationaleDialogShowing = false

    override fun initView(view: MainContract.View, savedState: Bundle?) {
        super.initView(view, savedState)
        locationView = view
        initialize()
    }

    protected fun initialize() {
        buildGoogleApiClient()
        locationView?.getLifeCycle()?.addObserver(object : LifecycleObserver {
            var isFirstResume = true
            @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
            fun onResume() {
                if (hasRequestedLocation && !isFirstResume && !isPermissionRationaleDialogShowing) {
                    initLocationRequestFlow()
                }
                isFirstResume = false
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_START)
            fun onStart() {
                googleApiClient.connect()
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun onStop() {
                stopLocationUpdates()
                googleApiClient.disconnect()
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                locationView?.getLifeCycle()?.removeObserver(this)
                locationView = null
            }
        })
    }

    protected fun buildGoogleApiClient() {
        googleApiClient = googleApiServices.getGoogleApiClient(context, this, this, LocationServices.API)
    }


    @SuppressLint("MissingPermission")
    fun startLocationUpdates() {
        if (!isLocationRequestInProgress) {
            isLocationRequestInProgress = true
            createLocationRequest()

            if (lastLocation == null) {
                lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient)

            }
            if (lastLocation == null) {
                if (googleApiClient.isConnected) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            googleApiClient,
                            locationRequest,
                            this
                    )
                } else {
                    isLocationRequestAwaitingConnection = true
                }
            } else {
                onLocationChanged(lastLocation)
            }
        }
    }

    @SuppressLint("MissingPermission")
    protected fun stopLocationUpdates() {
        if (googleApiClient.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    googleApiClient,
                    this
            )
        }
    }


    private fun checkLocationSettingsAndInitLocationRequest() {
        locationView?.apply {
            if (isLocationSettingsEnabled()) {
                dismissLocationsSnackbar()
                startLocationUpdates()
            } else {
                showLocationSettingsDisabledSnackBar()
            }
        }
    }


    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest?.apply {
            interval = UPDATE_INTERVAL_IN_MILLISECONDS
            fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

    }


    private fun hasLocationPermission(): Boolean {
        return android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M || ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(connectionHint: Bundle?) {
        Log.i(TAG, "Connected to GoogleApiClient")
        if (isLocationRequestAwaitingConnection) {
            isLocationRequestAwaitingConnection = false
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    googleApiClient,
                    locationRequest,
                    this
            )
        }

    }


    override fun onConnectionSuspended(cause: Int) {
        Log.i(TAG, "Connection suspended")
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.errorCode)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && hasLocationPermission()) {
                    checkLocationSettingsAndInitLocationRequest()
                } else {
                    locationView?.apply {
                        if (!shouldShowLocationPermissionRationale()) {
                            showPermissionDeniedSnackBar()
                        }
                    }
                }
            }
        }
    }

    override fun onLocationChanged(location: Location?) {
        location?.let {
            onLocationReceived(it)
            stopLocationUpdates()
            isLocationRequestInProgress = false
            hasRequestedLocation = false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                Activity.RESULT_OK -> {
                    startLocationUpdates()
                }
                Activity.RESULT_CANCELED -> locationView?.showLocationSettingsDisabledSnackBar()
            }
        }
    }

    fun initLocationRequestFlow() {
        hasRequestedLocation = true
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            locationView?.let {
                if (!deviceStoreRepository.hasLocationRationaleBeenShown()) {
                    deviceStoreRepository.setLocationRationaleShown(true)
                    it.showPermissionDialog(PERMISSIONS_REQUEST_LOCATION)
                    isPermissionRationaleDialogShowing = true
                } else if (it.shouldShowLocationPermissionRationale()) {
                    it.requestLocationPermission(PERMISSIONS_REQUEST_LOCATION)
                } else {
                    it.showPermissionDeniedSnackBar()
                }
            }
        } else {
            checkLocationSettingsAndInitLocationRequest()
        }
    }


    override fun onPermissionRationaleDismissed() {
        isPermissionRationaleDialogShowing = false
    }

}