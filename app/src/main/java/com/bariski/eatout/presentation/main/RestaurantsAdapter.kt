package com.bariski.eatout.presentation.main

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bariski.eatout.R
import com.bariski.eatout.presentation.common.toggleVisiblilty
import com.bariski.eatout.presentation.main.listener.OnThumbsDownListener
import com.bariski.eatout.presentation.models.Restaurant
import kotlinx.android.synthetic.main.item_restaurant.view.*

class RestaurantsAdapter(val listener: OnThumbsDownListener) : RecyclerView.Adapter<ViewHolder>() {

    @JvmField
    var data: ArrayList<Restaurant> = ArrayList()

    var loadingState = HashMap<Restaurant, Boolean>()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_restaurant, parent, false), listener, loadingState)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(viewholder: ViewHolder, pos: Int) {
        viewholder.bindData(data[pos])
    }

    fun setData(data: ArrayList<Restaurant>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun appendItems(data: ArrayList<Restaurant>, startIndex: Int, size: Int) {
        this.data = data
        notifyItemRangeInserted(startIndex, size)
    }

    fun resetLoadingState(restaurant: Restaurant) {
        val index = data.indexOf(restaurant)
        if (index >= 0) {
            loadingState[restaurant] = false
            notifyItemChanged(index)
        }
    }

    fun removeItem(restaurant: Restaurant) {
        val index = data.indexOf(restaurant)
        if (index >= 0) {
            loadingState.remove(data.removeAt(index))
            notifyItemRemoved(index)
        }
    }

}

class ViewHolder(val view: View, listener: OnThumbsDownListener, val loadingState: HashMap<Restaurant, Boolean>) : RecyclerView.ViewHolder(view) {

    lateinit var data: Restaurant

    init {
        view.thumbsDown.setOnClickListener {
            if (::data.isInitialized) {
                listener.onRestaurantThumbsDowned(data)
                loadingState[data] = true
                bindData(data)
            }
        }
    }

    fun bindData(data: Restaurant) {
        this.data = data
        with(view.thumbsDown) {
            val state = loadingState[data]
            if (state != null && state) startAnimation() else revertAnimation()
        }
        view.title.text = data.name
        view.description.text = data.location.address
        view.cuisine.text = data.categories[0].name
        view.distance.text = data.location.distanceString

        view.rating.toggleVisiblilty(data.rating != null)
        data.rating?.let {
            view.rating.text = "%.1f".format(it)
            view.rating.setBackgroundColor(ContextCompat.getColor(view.context, if (it >= 0 && it < 3.5) {
                R.color.red_500
            } else if (it >= 3.5 && it < 6.5) {
                R.color.button_yellow
            } else {
                R.color.green_400
            }))
        }

    }
}