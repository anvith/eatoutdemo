package com.bariski.eatout.presentation.main

import android.arch.lifecycle.Lifecycle
import android.content.Intent
import android.location.Location
import com.bariski.eatout.presentation.common.BasePresenter
import com.bariski.eatout.presentation.common.BaseView
import com.bariski.eatout.presentation.models.Restaurant
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import io.reactivex.Observable


class MainContract {

    interface View : BaseView<MainContract.Presenter>, LocationView {
        fun toggleMainProgress(makeVisible: Boolean)
        fun showRestaurants(restaurants: List<Restaurant>, shouldAppendData: Boolean)
        fun removeRestaurantFromView(restaurant: Restaurant)
        fun updateLocationStatus(status: String, animate: Boolean)
        fun toggleError(makeVisible: Boolean, title: String? = null, message: String? = null, showRetry: Boolean = true)
        fun getUserAuthRequest(): Observable<Any>
        fun makeLoginRequest(clientId: String, requestCode: Int)
        fun performTokenExchange(authCode: String, requestCode: Int, clientId: String, clientSecret: String)
        fun toggleAuthUi(isAuthenticated: Boolean)
        fun showMessage(msg: String)
        fun removeRestaurantFromViewFailed(restaurant: Restaurant)
    }

    abstract class Presenter : BasePresenter<View>(), LocationPresenter {
        abstract fun onRestaurantThumbsDowned(restaurant: Restaurant)
        abstract fun onRefresh()
        abstract fun hasLocationRationaleBeenShown(): Boolean
        abstract fun setLocationRationaleShown(value: Boolean)
        abstract fun onLocationReceived(location: Location)
    }


    interface LocationView {
        fun showLocationSettingsDisabledSnackBar()
        fun showPermissionDeniedSnackBar()
        fun shouldShowLocationPermissionRationale(): Boolean
        fun requestLocationPermission(requestCode: Int)
        fun showPermissionDialog(requestCode: Int)
        fun getLifeCycle(): Lifecycle
        fun isLocationSettingsEnabled(): Boolean
        fun dismissLocationsSnackbar()
    }

    interface LocationPresenter : GoogleApiClient.ConnectionCallbacks,
            GoogleApiClient.OnConnectionFailedListener,
            LocationListener {
        fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
        fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray)
        fun onPermissionRationaleDismissed
                ()

    }

}