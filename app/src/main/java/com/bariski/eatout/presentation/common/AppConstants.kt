package com.bariski.eatout.presentation.common


//Represents category code for restaurants foursquare api
const val SECTION_FOOD = "food"
const val DATE_FOURSQUARE_API = "20180910"
const val SEARCH_RADIUS = 20000
const val EMPTY_STRING = ""