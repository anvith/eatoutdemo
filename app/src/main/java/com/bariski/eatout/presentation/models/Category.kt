package com.bariski.eatout.presentation.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(val name: String) : Parcelable