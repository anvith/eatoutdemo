package com.bariski.eatout.presentation.models

data class RestaurantResponse(val isSuccess: Boolean, val data: List<Restaurant>?, val errorTitle: String? = null, val errorMessage: String? = null)