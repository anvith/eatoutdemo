package com.bariski.eatout.presentation.models

import android.location.Address

data class AddressResponse( val isSuccess: Boolean,val data: Address?)