package com.bariski.eatout.presentation.di

import android.content.Context
import com.google.android.gms.common.api.Api
import com.google.android.gms.common.api.GoogleApiClient
import javax.inject.Inject

/**
 * Wrapper class to facilitate mocking of google api client
 * **/
class GoogleApiServicesImpl @Inject constructor() : GoogleApiServices {

    override fun getGoogleApiClient(context: Context, connectionCallbacks: GoogleApiClient.ConnectionCallbacks, connectionFailedListener: GoogleApiClient.OnConnectionFailedListener, api: Api<out Api.ApiOptions.NotRequiredOptions>): GoogleApiClient {
        return GoogleApiClient.Builder(context)
                .addConnectionCallbacks(connectionCallbacks)
                .addOnConnectionFailedListener(connectionFailedListener)
                .addApi(api)
                .build()
    }
}