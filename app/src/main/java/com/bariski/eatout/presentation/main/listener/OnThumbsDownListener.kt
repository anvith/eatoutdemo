package com.bariski.eatout.presentation.main.listener

import com.bariski.eatout.presentation.models.Restaurant

interface OnThumbsDownListener {

    fun onRestaurantThumbsDowned(restaurant: Restaurant)
}