package com.bariski.eatout.presentation.di

import android.content.Context
import com.google.android.gms.common.api.Api
import com.google.android.gms.common.api.GoogleApiClient

/**
 * Wrapper class to facilitate mocking of google api client
 * **/
interface GoogleApiServices {
    fun getGoogleApiClient(context: Context, connectionCallbacks: GoogleApiClient.ConnectionCallbacks, connectionFailedListener: GoogleApiClient.OnConnectionFailedListener, api: Api<out Api.ApiOptions.NotRequiredOptions>): GoogleApiClient
}