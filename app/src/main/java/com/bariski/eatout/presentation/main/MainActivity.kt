package com.bariski.eatout.presentation.main

import android.arch.lifecycle.Lifecycle
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.bariski.eatout.R
import com.bariski.eatout.presentation.common.toggleVisiblilty
import com.bariski.eatout.presentation.main.listener.OnThumbsDownListener
import com.bariski.eatout.presentation.models.Restaurant
import com.foursquare.android.nativeoauth.FoursquareOAuth
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : LocationActivity<MainContract.Presenter>(), MainContract.View, OnThumbsDownListener {

    private lateinit var adapter: RestaurantsAdapter

    private var restaurants = ArrayList<Restaurant>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        list.layoutManager = LinearLayoutManager(this)
        adapter = RestaurantsAdapter(this)
        list.adapter = adapter
        listContainer.setOnRefreshListener {
            getPresenter().onRefresh()
            listContainer.isRefreshing = false
        }
        retry.setOnClickListener {
            getPresenter().onRefresh()
        }
        getPresenter().initView(this, savedInstanceState)
    }

    override fun toggleMainProgress(makeVisible: Boolean) {
        progress.toggleVisiblilty(makeVisible)
    }

    override fun getUserAuthRequest(): Observable<Any> {
        return RxView.clicks(login)
    }

    override fun showRestaurants(restaurants: List<Restaurant>, shouldAppendData: Boolean) {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
            container.setBackgroundColor(ContextCompat.getColor(this, R.color.background_grey))
            if (shouldAppendData) {
                list.layoutAnimation = null
                this.restaurants.addAll(restaurants)
                adapter.appendItems(this.restaurants, this.restaurants.size, restaurants.size)
            } else {
                val controller = AnimationUtils.loadLayoutAnimation(list.context, R.anim.layout_animation_fall_down)
                list.layoutAnimation = controller
                this.restaurants.clear()
                this.restaurants.addAll(restaurants)
                adapter.setData(this.restaurants)
                list.scheduleLayoutAnimation()

            }
        }
    }

    override fun removeRestaurantFromView(restaurant: Restaurant) {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED)) {
            adapter.removeItem(restaurant)
            if (restaurants.size == 0) {
                getPresenter().onRefresh()
            }
        }
    }

    override fun onRestaurantThumbsDowned(restaurant: Restaurant) {
        getPresenter().onRestaurantThumbsDowned(restaurant)
    }

    override fun toggleError(makeVisible: Boolean, title: String?, message: String?, showRetry: Boolean) {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED)) {
            errorContainer.toggleVisiblilty(makeVisible)
            if (makeVisible) {
                container.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
                errorTitle.text = title
                errorMessage.text = message
                retry.toggleVisiblilty(showRetry)
            }
        }
    }

    override fun removeRestaurantFromViewFailed(restaurant: Restaurant) {
        if (isAlive()) {
            adapter.resetLoadingState(restaurant)
        }
    }


    override fun updateLocationStatus(status: String, animate: Boolean) {
        if (isAlive()) {
            locationStatus.text = status
            locationStatus.clearAnimation()
            if (animate) {
                val animation = AlphaAnimation(0.5f, 1.0f)
                animation.duration = 400
                animation.repeatMode = Animation.REVERSE
                animation.repeatCount = Animation.INFINITE
                locationStatus.startAnimation(animation)
            }
        }
    }

    override fun makeLoginRequest(clientId: String, requestCode: Int) {
        if (isAlive()) {
            val intent = FoursquareOAuth.getConnectIntent(this, clientId)
            startActivityForResult(intent, requestCode)
        }
    }


    override fun showMessage(msg: String) {
        if (isAlive()) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        }
    }

    override fun performTokenExchange(authCode: String, requestCode: Int, clientId: String, clientSecret: String) {
        if (isAlive()) {
            val intent = FoursquareOAuth.getTokenExchangeIntent(this, clientId, clientSecret, authCode)
            startActivityForResult(intent, requestCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (isAlive()) {
            super.onActivityResult(requestCode, resultCode, data)
            getPresenter().onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun toggleAuthUi(isAuthenticated: Boolean) {
        if (isAlive()) {
            login.setImageResource(if (isAuthenticated) R.drawable.ic_logout else R.drawable.ic_login)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.apply {
            putParcelableArrayList("restaurants", restaurants)
            putString("locationStatus", locationStatus.text.toString())
        }
    }


}
