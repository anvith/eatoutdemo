package com.bariski.eatout.presentation.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Restaurant(
        val id: String,
        val name: String,
        val rating: Float?,
        val location: Location,
        val categories: List<Category>
) : Parcelable