package com.bariski.eatout.presentation.common

import android.os.Bundle
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<T> {

    val disposable = CompositeDisposable()
    var view: T? = null

    open fun initView(view: T, savedState: Bundle?) {
        this.view = view
    }

    open fun releaseView() {
        view = null
        disposable.clear()
    }
}