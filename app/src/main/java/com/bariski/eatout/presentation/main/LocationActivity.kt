package com.bariski.eatout.presentation.main

import android.Manifest
import android.app.AlertDialog
import android.arch.lifecycle.Lifecycle
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.view.ContextThemeWrapper
import android.text.TextUtils
import com.bariski.eatout.R
import com.bariski.eatout.presentation.common.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

/****
 * This is an abstraction that deals with location api,
 * Slightly better approach maybe to have a separate component that abstracts this further from activity
 */
abstract class LocationActivity<T : Any> : BaseActivity<T>(), MainContract.LocationView {

    protected lateinit var locationPresenter: MainContract.LocationPresenter

    private var snackbar: Snackbar? = null

    private lateinit var rationaleDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationPresenter = viewPresenter as LocationPresenter
    }

    override fun getLifeCycle(): Lifecycle {
        return lifecycle
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        locationPresenter.onActivityResult(requestCode, resultCode, data)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        locationPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun hasLocationPermission(): Boolean {
        return android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }


    override fun showPermissionDialog(requestCode: Int) {
        if (!::rationaleDialog.isInitialized) {
            rationaleDialog = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AppDialog))
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.text_location_permission)
                    .setPositiveButton(R.string.ok) { _, _ ->
                        requestLocationPermission(requestCode)
                        locationPresenter.onPermissionRationaleDismissed()
                        rationaleDialog.dismiss()
                    }
                    .setCancelable(false)
                    .create()

        }
        rationaleDialog.show()


    }


    override fun showPermissionDeniedSnackBar() {
        snackbar?.dismiss()
        snackbar = Snackbar.make(container, R.string.permissions_not_granted,
                Snackbar.LENGTH_INDEFINITE).setAction(R.string.permissions_settings) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
        }
        snackbar?.show()
    }

    override fun isLocationSettingsEnabled(): Boolean {
        var locationMode = 0
        val locationProviders: String

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(contentResolver, Settings.Secure.LOCATION_MODE)

            } catch (e: Settings.SettingNotFoundException) {
                return false
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF

        } else {
            locationProviders = Settings.Secure.getString(contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
            return !TextUtils.isEmpty(locationProviders)
        }
    }

    override fun requestLocationPermission(requestCode: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCode)
    }

    override fun showLocationSettingsDisabledSnackBar() {
        snackbar?.dismiss()
        snackbar = Snackbar.make(container, R.string.error_location_settings_disabled,
                Snackbar.LENGTH_INDEFINITE).setAction(R.string.permissions_settings) {
            startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
        snackbar?.show()
    }

    override fun dismissLocationsSnackbar() {
        snackbar?.dismiss()
    }

    override fun shouldShowLocationPermissionRationale(): Boolean {
        return ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
    }

}