package com.bariski.eatout.domain.repository

interface UserRepository {
    fun isLoggedIn(): Boolean
    fun setOAuthToken(value: String)
    fun getOAuthToken(): String
}