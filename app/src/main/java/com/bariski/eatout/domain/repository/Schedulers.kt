package com.bariski.eatout.domain.repository

import io.reactivex.Scheduler

interface Schedulers {

    fun io(): Scheduler
    fun ui(): Scheduler
    fun compute(): Scheduler
}