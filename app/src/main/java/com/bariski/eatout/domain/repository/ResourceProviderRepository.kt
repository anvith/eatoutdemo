package com.bariski.eatout.domain.repository

interface ResourceProviderRepository {

    fun getString(id: Int): String
}