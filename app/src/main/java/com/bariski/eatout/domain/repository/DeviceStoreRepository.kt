package com.bariski.eatout.domain.repository

interface DeviceStoreRepository {

    fun hasLocationRationaleBeenShown(): Boolean

    fun setLocationRationaleShown(value: Boolean)
}