package com.bariski.eatout.domain.repository

import android.location.Address
import android.location.Location
import io.reactivex.Single

interface LocationRepository {

    fun getAddressFromLocation(location: Location): Single<Address?>
}