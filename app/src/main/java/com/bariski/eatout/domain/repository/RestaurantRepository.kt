package com.bariski.eatout.domain.repository

import com.bariski.eatout.presentation.models.Restaurant
import io.reactivex.Completable
import io.reactivex.Single

interface RestaurantRepository {
    fun getRestaurants(latLong: String): Single<List<Restaurant>>
    fun dislikeVenue(venueId: String): Completable
}