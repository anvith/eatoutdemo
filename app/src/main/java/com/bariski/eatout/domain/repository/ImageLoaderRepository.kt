package com.bariski.eatout.domain.repository

import com.bariski.eatout.domain.models.ImageRequest

interface ImageLoaderRepository {

    fun loadImage(request: ImageRequest)
    fun clearCache()
}