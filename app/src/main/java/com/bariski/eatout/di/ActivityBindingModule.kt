package com.bariski.eatout.di

import com.bariski.eatout.presentation.di.MainModule
import com.bariski.eatout.presentation.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun mainActivity(): MainActivity


}