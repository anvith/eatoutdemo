package com.bariski.eatout.di

import android.app.Application
import com.bariski.eatout.FoodOrderApplication
import com.bariski.eatout.data.di.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule


@ApplicationScope
@Component(modules = [AppModule::class, NetworkModule::class, ResourcesModule::class, UserModule::class,LocationModule::class, StorageModule::class, SchedulerModule::class, RestaurantsModule::class, ActivityBindingModule::class, AndroidSupportInjectionModule::class, AndroidInjectionModule::class])
interface AppComponent {

    fun inject(application: FoodOrderApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

}