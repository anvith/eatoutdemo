package com.bariski.eatout.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module


@Module
abstract class AppModule {

    @Binds
    @ApplicationScope
    abstract fun bindAppContext(application: Application): Context


}