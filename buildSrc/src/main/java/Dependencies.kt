object Version {
    val min_sdk = 19
    val target_sdk = 28
    val compile_sdk = 28

    val version_code = 1
    val version_name = "1.0"

    val picasso = "2.5.2"
    val picasso_downloader = "1.1.0"
    val kotlin = "1.2.51"
    val support_lib = "28.0.0-rc01"
    val constraint_layout = "1.1.2"
    val rxjava = "2.2.1"
    val rxandroid = "2.1.0"
    val rxkotlin = "2.2.0"
    val android_testrunner = "1.0.2"
    val espresso = "3.0.2"
    val junit = "4.12"
    val android_gradle = "3.1.3"
    val rxbinding = "2.1.1"
    val moshi = "1.6.0"
    val dagger = "2.16"
    val retrofit = "2.3.0"
    val gms = "15.0.0"
    val rx_location = "1.0.5"
    val rx_permission = "0.10.2"
    val foursquare = "1.0.3"
    val progress_button = "1.14.0"
    val mockito = "2.10.0"
    val assertj = "3.8.0"
    val mockito_kotlin = "2.0.0-RC2"

}


object Deps {

    val android_gradle = "com.android.tools.build:gradle:${Version.android_gradle}"
    val kotlin_gradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Version.kotlin}"
    val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Version.kotlin}"

    val rxjava = "io.reactivex.rxjava2:rxjava:${Version.rxjava}"
    val rxandroid = "io.reactivex.rxjava2:rxandroid:${Version.rxandroid}"
    val rxkotlin = "io.reactivex.rxjava2:rxkotlin:${Version.rxkotlin}"
    val rxbindings = "com.jakewharton.rxbinding2:rxbinding:${Version.rxbinding}"
    val rx_call_adapter = "com.squareup.retrofit2:adapter-rxjava2:${Version.retrofit}"

    val appcompat_v7 = "com.android.support:appcompat-v7:${Version.support_lib}"
    val recyclerview = "com.android.support:recyclerview-v7:${Version.support_lib}"
    val cardview = "com.android.support:cardview-v7:${Version.support_lib}"
    val design = "com.android.support:design:${Version.support_lib}"
    val constraint_layout = "com.android.support.constraint:constraint-layout:${Version.constraint_layout}"

    val gms_location = "com.google.android.gms:play-services-location:${Version.gms}"

    val junit = "junit:junit:${Version.junit}"
    val espresso = "com.android.support.test.espresso:espresso-core:${Version.espresso}"
    val kotlin_junit = "org.jetbrains.kotlin:kotlin-test-junit:${Version.kotlin}"
    val android_test_runner = "com.android.support.test:runner:${Version.android_testrunner}"
    val mockito = "org.mockito:mockito-core:${Version.mockito}"
    val assertj = "org.assertj:assertj-core:${Version.assertj}"
    val mockito_kotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:${Version.mockito_kotlin}"

    val dagger_core = "com.google.dagger:dagger:${Version.dagger}"
    val dagger_android = "com.google.dagger:dagger-android:${Version.dagger}"
    val dagger_android_support = "com.google.dagger:dagger-android-support:${Version.dagger}" // if you use the support libraries
    val dagger_core_annotation_processor = "com.google.dagger:dagger-compiler:${Version.dagger}"
    val dagger_android_annotation_processor = "com.google.dagger:dagger-android-processor:${Version.dagger}"

    val moshi_kotlin = "com.squareup.moshi:moshi-kotlin:${Version.moshi}"
    val retrofit = "com.squareup.retrofit2:retrofit:${Version.retrofit}"

    val picasso = "com.squareup.picasso:picasso:${Version.picasso}"
    val picasso_downloader = "com.jakewharton.picasso:picasso2-okhttp3-downloader:${Version.picasso_downloader}"
    val converter = "com.squareup.retrofit2:converter-moshi:${Version.retrofit}"

    val foursquare = "com.foursquare:foursquare-android-oauth:${Version.foursquare}"

    //Button that morphs into a loader a custom UI.
    val progress_button = "br.com.simplepass:loading-button-android:${Version.progress_button}"


    val network = listOf(retrofit, moshi_kotlin, picasso, picasso_downloader, converter)
    val annotation_processors = listOf(dagger_android_annotation_processor, dagger_core_annotation_processor)
    val dagger = listOf(dagger_core, dagger_android, dagger_android_support)
    val rx = listOf(rxjava, rxandroid, rxkotlin, rxbindings, rx_call_adapter)
    val supportLibs = listOf(appcompat_v7, recyclerview, constraint_layout, cardview, design)
    val custom_ui_libs = listOf(progress_button)
    val gms = listOf(gms_location)
    val testingLibs = listOf(junit, mockito, mockito_kotlin, assertj, kotlin_junit)
}